#cpp1z group awesome

 **排名不分先后** 

推荐项目为笔者试用过的,汇总的未必全面或者准确，如有不妥之处（网名是否需要隐去？），请指出，我将第一时间进行修正

如有需要添加的项目，可写在评论里，我将后续追加

大家别忘了给他们star噢！ :fa-asterisk: 


江南（群主）:

主页：[https://github.com/qicosmos](https://github.com/qicosmos)

社区：[purecpp.org](http://purecpp.org)

推荐项目：

rest_rpc:[https://github.com/qicosmos/rest_rpc](ttps://github.com/qicosmos/rest_rpc) 使用c++实现rest风格的rpc接口，不过对编译器要求有点高

其他项目(没有列出地址的github上可以搜到):

~~Kapok:更好的序列化库，c++14开发，header-only，简洁高效。~~

iguana(2018.3.16更新)：作者推荐所以去掉了Kapok换成了iguana

cinatra(2018.3.16更新):仿sinatra,让C++写http_server变得爽起来,目前社区很活跃噢

ormpp(2018.3.16更新):ormpp是modern c++(c++11/14/17)开发的 **很酷的** ORM库，目前支持了三种数据库：mysql, postgresql和sqlite,



Mr.li(大名顶顶的于老师,魅族高级架构师):

主页：[https://github.com/yyzybb537](https://github.com/yyzybb537)

推荐项目：

libgo:[https://github.com/yyzybb537/libgo](https://github.com/yyzybb537/libgo) libgo是一个使用C++11编写的协作式调度的stackful协程库
其他项目:
Xedis,go写的支持redis命令的kv数据据，支持定制化



狐狸发:

主页:[https://github.com/lordoffox](https://github.com/lordoffox)

推荐项目:

ajson [https://github.com/lordoffox/ajson](https://github.com/lordoffox/ajson) 将一个C++结构体，序列化到dom，然后将dom再次序列化到文本



Scarface

主页：[https://github.com/roy2220](https://github.com/roy2220)

推荐项目：

siren [https://github.com/roy2220/siren](https://github.com/roy2220/siren) 一个小型的linux的网络库，里面的hash_table可以把原本到2^N 临界才rehash的操作分摊到每次插入上



网事如风：

主页:[https://github.com/avdbg](https://github.com/avdbg)

推荐项目：
cinatra [https://github.com/topcpporg/cinatra](https://github.com/topcpporg/cinatra) A sinatra inspired modern c++ web framework,你可以用c++11写像sinatra一样风格的web代码了



xsky:

主页：[https://github.com/0xsky](https://github.com/0xsky)

推荐项目：

xredis:[https://github.com/0xsky/xredis](https://github.com/0xsky/xredis) C++ Redis client, support the data slice storage, connection pool, read/write separation. 支持切片存储，连接池，读写分离的redis的封装



风驰：

主页：[https://git.oschina.net/zsxxsz](https://git.oschina.net/zsxxsz)

推荐项目：

acl:[https://git.oschina.net/zsxxsz/acl](https://git.oschina.net/zsxxsz/acl) acl 工程是一个跨平台（支持LINUX，WIN32，Solaris，MacOS，FreeBSD）的网络通信库及服务器编程框架



sarrow104/vimrc：

主页：https://github.com/sarrow104

推荐项目：
~~varlisp:https://github.com/sarrow104/varlisp a lisp syntax like mini interpreter，一个迷你型的lisp解释器，里面各种模块都有：arithmetic,bitop,logic,encoding,string,http,regex,shell,map/reduce/filter~~
应作者要求，去掉了推荐项目(2018.3.16)



Θ§奏之章℡

主页：[https://github.com/mm304321141](https://github.com/mm304321141)

推荐项目：

zzz_lib:https://github.com/mm304321141/zzz_lib sbtree_map,sbtree_set,bpptree_map,bpptree_set,chash_map,chash_set,segment_array 各种树结构的容器，榨取每一点性能，够你玩的了



陈高原

主页:https://git.oschina.net/.cgy.com

推荐项目：

ForeachKit:https://git.oschina.net/.cgy.com/CPP.git 可用于递归遍历容器



黑风

主页:https://github.com/alucard-dracula

推荐项目:https://github.com/alucard-dracula/yggdrasil yggdrasil是一个基于c++的游戏擎库，服务端部份包括很多实用的功能，包括脚本语言绑定，代理，序列化等常用功能，而且是跨平台的，最重要的是，它兼容性很好，能在C++03/11+项目中使用



[ ^ @_@ ^ ]

主页:https://github.com/yuanzhubi/

推荐项目:https://github.com/yuanzhubi/cort_proto A C++03 implement stackless coroutine. But also support stackful coroutine, tcp rpc operation, static hook(by adding hook function into static_hook.sh).C++03弄的不错，满足低版编译器的需求，据说是作者这几年花功夫最大的一个开源项目(2018.3.16)

https://github.com/yuanzhubi/call_in_stack  call_in_stack不是协程库，它可以帮助协程减少内存使用，它无成本的允许在同一个线程中的所有协程间通过全局内存“共享栈”，只要这块栈内存上执行的函数不会发生协程切换(2018.3.16更新介绍)

